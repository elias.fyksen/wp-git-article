<?php
defined('ABSPATH') or die( 'No script kiddies please!' );




# Adding content filter

function wpga_add_filter_content($content) {

	$id = get_the_ID();

	if( get_post_type( $id ) != 'post' )
		return $content;

	if( empty( get_post_meta( $id, 'git_article_raw_link', true ) ) )
		return $content;

	global $wpga_path_root;

	ob_start();

	include( plugin_dir_path( __FILE__ ) . '../public/html/content.php' );

	$content = ob_get_contents();
	ob_clean();
	ob_end_flush();

	return $content;
}

add_filter( 'the_content', 'wpga_add_filter_content' );




# Enqueueing content script

function wpga_enqueue_script_content() {

	wp_enqueue_script(
		'wpga-script-content',
		plugin_dir_url( __FILE__ ) . '../public/js/content.js',
		array( 'jquery' ),
		false,
		true
	);
	wp_localize_script(
		'wpga-script-content',
		'wpga_data',
            	array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'post_id' => get_the_ID()
		)
	);
}

add_action( 'wp_enqueue_scripts', 'wpga_enqueue_script_content' );




# Enqueueing font awsome css

function wpga_enqueue_style_fa(){

	wp_enqueue_style(
		'wpga-style-fa',
		'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
		array(),
		false,
		'all'
	);
}

add_action( 'wp_enqueue_scripts', 'wpga_enqueue_style_fa' );
?>
