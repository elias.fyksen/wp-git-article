<?php

function wpga_git_article() {

	require_once( plugin_dir_path( __FILE__ ) . 'Parsedown.php' );

	$id = $_POST['post-id'];

	$link_raw = get_post_meta($id, 'git_article_raw_link', true);
	$link_git = get_post_meta($id, 'git_article_git_link', true);

	$html = file_get_contents($link_raw);

	$parsedown = new Parsedown();
	$html = $parsedown->text($html);
	$html = do_shortcode($html);

	$content = json_encode(array(
		'html' => $html,
		'link' => $link_git
	));

	echo $content;

	wp_die();
}

add_action( 'wp_ajax_wpga_git_article', 'wpga_git_article' );
add_action( 'wp_ajax_nopriv_wpga_git_article', 'wpga_git_article' );
