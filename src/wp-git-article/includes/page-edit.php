<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );



function render_git_article_metabox( ){

	$raw_link = get_post_meta(get_the_ID(), 'git_article_raw_link', true);
	$git_link = get_post_meta(get_the_ID(), 'git_article_git_link', true);

	?>

	<input type="text" name="git-article-raw-link" placeholder="Raw link" value="<?php echo $raw_link;?>">
	<input type="text" name="git-article-git-link" placeholder="Git link" value="<?php echo $git_link;?>">

	<?php
}

function adding_git_article_metabox( $post_type, $post ) {
    add_meta_box(
        'git-article-metabox',
        __( 'Git Article' ),
        'render_git_article_metabox',
        'post',
        'side',
        'high'
    );
}

add_action( 'add_meta_boxes', 'adding_git_article_metabox', 10, 2 );







function git_article_save_postdata($post_id)
{
    if (array_key_exists('git-article-raw-link', $_POST)) {
        update_post_meta(
            $post_id,
            'git_article_raw_link',
            $_POST['git-article-raw-link']
        );
    }

    if (array_key_exists('git-article-git-link', $_POST)) {
        update_post_meta(
            $post_id,
            'git_article_git_link',
            $_POST['git-article-git-link']
        );
    }
}
add_action('save_post', 'git_article_save_postdata');

?>
