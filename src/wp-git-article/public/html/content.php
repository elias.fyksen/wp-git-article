<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
?>
<a id="wpga-link" target="_blank" href="#">
	<div style="margin-bottom: 40px;font-size: 20px;padding: 10px; color: black;border: 3px solid red;border-radius: 5px;background: #faa; cursor: pointer;">
		<p>This article is written as a <strong>git article</strong>. That means that <strong>you</strong> can suggest changes via git, by clicking on this box.</p>
	</div>
</a>
<div id="wpga-content" style="text-align:center;">
	<i class="fa fa-cog fa-spin fa-5x"></i><br><br>
	<span>Loading git article...</span>
</div>
