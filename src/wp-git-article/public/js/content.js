//Content script

(function ( $ ){

	var data = {
		'action': 'wpga_git_article',
		'post-id': wpga_data.post_id
	};

	$.post(
		wpga_data.ajax_url,
		data,
		function (response) {

			response	= JSON.parse(response);
			html		= response.html;
			link		= response.link;

			$('#wpga-link')
				.attr('href', link);

			$('#wpga-content')
				.html(html);
			$('#wpga-content')
				.css('text-align', 'left');
		}
	);

})( jQuery );

