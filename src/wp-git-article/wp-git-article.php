<?php
/**
 * Plugin Name: WP Git Article
 * Plugin URI: N/A
 * Description: A wordpress plugin for making articles in markdown/git, and then embedding them on a wordpress site
 * Version: 0.0.0
 * Author: Elias F. Fyksen
 * Author URI: http://fyksen.net
 * License: GPL2
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( true ){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}


if( is_admin() ) {

	require_once( plugin_dir_path( __FILE__ ) . 'includes/page-edit.php' );

} else {

	require_once( plugin_dir_path( __FILE__ ) . 'includes/content-display.php');

}

require_once( plugin_dir_path( __FILE__ ) . 'includes/ajax-content.php');


?>
